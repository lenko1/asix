:doctype: article
:encoding: utf-8
:lang: ca

= Act8 NF1UF1. Llenguatges de marques lleugers =
== M4 Llenguatge de marques ==

1. Alfa.
2. Beta.
3. Charly.

* Alfa
* Beta
* Charly

_Alfa_

*Beta*

[LINK IES SABADELL](https://agora.xtec.cat/ies-sabadell/)

```python
s = "Python syntax highlighting"
print s
```
image:https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTr2kG-ysAD7Ui-LsNtDePvNhuZEeBXfrlEQReJ7JcYQPdDlCqabA
[Logo Title Text 1]


| ===
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| ===

[NOTE]
.A NOTE admonition block
=====================================================================
Qui in magna commodo, est labitur dolorum an. Est ne magna primis
adolescens.

. Fusce euismod commodo velit.
. Vivamus fringilla mi eu lacus.
  .. Fusce euismod commodo velit.
  .. Vivamus fringilla mi eu lacus.
. Donec eget arcu bibendum
  nunc consequat lobortis.
=====================================================================

1. Java.
+
Codi de Java dins de llista numerada.
+

[source,python]
----
s = "Python syntax highlighting"
print s
----
2. Python
+
Codi de Python
+

3. C++
+
Codi C++
+